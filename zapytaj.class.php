<?php

/*
* zapytaj.class.php
*
* Copyright 2011-2012 Sebastian Korotkiewicz <sebastian@korotkiewicz.eu>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

class _zapytaj
{

		/*
		 * Funkcja logowania na Zapytaj.onet.pl
		 *
		 * */

	function loguj ($login, $haslo) {
	    $curlchanel = curl_init("http://zapytaj.onet.pl/login.html");
	    curl_setopt($curlchanel, CURLOPT_USERAGENT, SETUSERAGENT);
	    curl_setopt($curlchanel, CURLOPT_COOKIEJAR, 'cookie.txt');
	    curl_setopt($curlchanel, CURLOPT_COOKIEFILE, 'cookie.txt');
	    curl_setopt($curlchanel, CURLOPT_HEADER, 0);
	    curl_setopt($curlchanel, CURLOPT_TIMEOUT, 6);
	    curl_setopt($curlchanel, CURLOPT_RETURNTRANSFER, 0);
	    curl_setopt($curlchanel, CURLOPT_POSTFIELDS, trim("login%5Blogin%5D=$login&login%5Bpassword%5D=$haslo&_ws=1920x1080x24&_p=4b9e55ba47ed9a4512b5054fe5e3bd86&_f=858a2bcbaa6a9f1b47fc362274e9ba56&_c=eea32fa0afd17e55c16508f35f10ce4c&zaloguj=+++Zaloguj+++"));

	    $wynik=curl_exec($curlchanel);
	    if ($wynik!=1) {
		echow("Nie udalo sie zalogowac");
		die;
	    }
	    curl_close($curlchanel);
	}

		/*
		 * Funkcja pobierania tokeny aktualnie zalogowanego użytkownika
		 *
		 * */

	function pobierztoken() {

		$a = curl_init("http://zapytaj.onet.pl/Category/007,008/AddAnswer,1,14869299.html");
		curl_setopt($a, CURLOPT_USERAGENT, SETUSERAGENT);
	   	curl_setopt($a, CURLOPT_COOKIEJAR, 'cookie.txt');
		curl_setopt($a, CURLOPT_COOKIEFILE, 'cookie.txt');
		curl_setopt($a, CURLOPT_RETURNTRANSFER, 1);
		$wynik=curl_exec($a);
		       curl_close($a);
		preg_match('/<input type="hidden" name="token" value="(.+)"\/>/i', $wynik, $token);
			$this->nasztoken = $token[1];
		 return $token[1];
	    }


        function pobierztokenShoutbox() {
            $curlchanel = curl_init("http://zapytaj.onet.pl/");
            curl_setopt($curlchanel, CURLOPT_USERAGENT, SETUSERAGENT);
            curl_setopt($curlchanel, CURLOPT_COOKIEJAR, 'cookie.txt');
            curl_setopt($curlchanel, CURLOPT_COOKIEFILE, 'cookie.txt');
            curl_setopt($curlchanel, CURLOPT_HEADER, 0);
            curl_setopt($curlchanel, CURLOPT_TIMEOUT, 6);
            curl_setopt($curlchanel, CURLOPT_RETURNTRANSFER, 1);
            $wynik=curl_exec($curlchanel);
            preg_match("#token=([a-z0-9]{32})#", $wynik, $token);
            curl_close($curlchanel);
		$this->sbtoken = $token[1];
            return $token[1];
        }


	 function curl($url, $post=false){
		$c = curl_init($url);

		curl_setopt($c, CURLOPT_USERAGENT, SETUSERAGENT);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_MAXREDIRS, 5);
		curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($c, CURLOPT_COOKIEJAR, 'cookie.txt');
		curl_setopt($c, CURLOPT_COOKIEFILE, 'cookie.txt');
		//curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_TIMEOUT, 6);

		if($post!==false){
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $post);
		}

		$r = curl_exec($c);

		$this->last_url=curl_getinfo($c, CURLINFO_EFFECTIVE_URL);

		$this->gets++;
		return $r;
	}






		/*
		 * Funkcja oddawania głosu (łapki) na pytania.
		 *
		 * */

	function vote($question, $lapka) {
		$this->curl("http://zapytaj.onet.pl/Ajax/Question/vote.html", trim("question=$question&thumb=$lapka"));
	}


		/*
		 * Funkcja zapraszania znajomych do przyjaciół
		 *
		 * */

	function addFriend($id) {
		$this->curl("http://zapytaj.onet.pl/Friends/$id/add.html", trim("token=".$this->nasztoken.""));
	}

		/*
		 * Funkcja zadawania pytania
		 *
		 * */

	function zapytaj($pytanie) {
		$this->curl("http://zapytaj.onet.pl/question.php?id=1", trim("type_radio=normal&req_question=$pytanie&req_text=&ankietaodp1=&ankietaodp2=&ankietaodp3=&ankietaodp4=&ankietaodp5=&ankietaodp6=&ankietaodp7=&ankietaodp8=&ankietaodp9=&ankietaodp10=&video_link=&tags=&searchCategory=&select1=001&select2=001%2C003&numer=001%2C003&token=".$this->nasztoken."&AddQuestion=Zapytaj%21"));
	}


		/*
		 * Funkcja odpowiadania na pytanie
		 *
		 * */

	function odpowiedz($pytanieId, $odpowiedz) {
		$this->curl("http://zapytaj.onet.pl/Category/001,003/Added,1,$pytanieId.html", trim("req_question=$pytanieId&req_category=001%2C003&req_text=$odpowiedz&isrich=0&video_link=&token=".$this->nasztoken.""));
	}


		/*
		 * Funkcja pisania na Shoutboxie
		 *
		 * */

	function shoutbox($tresc) {
		$this->curl("http://zapytaj.onet.pl/ajax_shoutbox.php", trim("tresc=$tresc&send=1&token=".$this->sbtoken));
	}


		/*
		 * Funkcja wysyłania prywatnych wiadomości do znajomych
		 *
		 * */


	function sendPM($idUserDoKogo, $temat, $tresc) {
		$this->curl("http://zapytaj.onet.pl/Message/create.html", trim("friend=$idUserDoKogo&subject=$temat&tresc=$tresc&token=".$this->nasztoken."&submit=Wy%C5%9Blij+wiadomo%C5%9B%C4%87"));
	}


		/*
		 * Funkcja usuwania użytkownika z czarnej listy
		 *
		 * */

	function unblacklist($uid) {
		$this->curl("http://zapytaj.onet.pl/Blacklist/$uid/remove.html", trim("token=".$this->nasztoken."&x=16&y=16"));
	}



		/*
		 * Funkcja dodawania użytkownika do czarnej listy
		 *
		 * */

	function blacklist($uid) {
		$this->curl("http://zapytaj.onet.pl/Blacklist/$uid/add.html", "token=".$this->nasztoken);
	}



		/*
		 * Funkcja oddawania głosu w Ankiecie
		 *
		 * */

	function zaglosujAnkieta($odpId, $idAkieta) {
		$this->curl("http://zapytaj.onet.pl/Category/015,006/VoteAnkieta,1,$idAkieta.html", "choice=$odpId&questionAnkieta=$idAkieta&goAnkieta=1");
	}


		/*
		 * Funkcja do oddawania głosów na Sondę
		 *
		 * */

	function zaglosujSonda($id, $odp){
		$this->curl("http://zapytaj.onet.pl/Category/024,003/AddVote,1,$id,$odp.html", "http://zapytaj.onet.pl/Category/024,003/AddVote,1,$id,$odp.html");
	}


		/*
		 * Funkcja wysyłania komentarza do użykownika
		 *
		 * */

	function add_comment($id, $content){
		$this->curl("http://zapytaj.onet.pl/Profile/$id/comment/add.html", 'trescComm='.rawurlencode($content).'&token='.$this->nasztoken);
	}


		/*
		 *
		 * Funkcja tworząca nowy klub
		 *
		 * */

	function newClub($nazwaKlubu, $address, $desc){
		$this->curl("http://zapytaj.onet.pl/Klub/nowy.html", "name=$nazwaKlubu&address=$address&description=$desc&ZalozKlub=Dalej");
	}



		/*
		 * Funkcja oznaczająca odpowiedź jako najlepsza
		 *
		 * */

	function bestAnswer($idOdpowiedzi){
		$this->curl("http://zapytaj.onet.pl/Best/Answer/$idOdpowiedzi/set.html", 'token='.$this->nasztoken.'&x=43&y=15');
	}


		/*
		 * Funkcja usuwająca komentarz z profilu
		 *
		 * */

	function delComment($iduser, $idcomment){
		$this->curl("http://zapytaj.onet.pl/comment/$iduser/remove.html", 'delcomment='.$idcomment.'&type=profile');
	}

}

?>
